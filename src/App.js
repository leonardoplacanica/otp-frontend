import {useState} from "react";
import './App.css';
import {Button, TextField, CircularProgress} from "@material-ui/core";
const proxy = "https://2j7tyt6k77.execute-api.us-east-2.amazonaws.com/prod";

function App() {
  const onSubmit = async(e) => {
    e.preventDefault()
    setLoading(true);
    const res = await fetch(proxy + "/email", {
      method: "POST",
      body: JSON.stringify({
        email
      })
    });
    const data = await res.json();
    if(data.email) setStep(1);
    setLoading(false);
  }

  const onSubmitOtp = async(e) => {
    e.preventDefault()
    setLoading(true);
    const res = await fetch(proxy + "/check-otp", {
      method: "POST",
      body: JSON.stringify({
        email,
        otp
      })
    });
    const data = await res.json();
    if(data.user) setStep(2);
    else setStep(3);
    setLoading(false);
  }

  const [email, setEmail] = useState("")
  const [loading, setLoading] = useState(false)
  const [otp, setOtp] = useState("")
  const [step, setStep] = useState(0);

  return (
    <div className="App">
      <header className="App-header">
        {step === 0 && <>
        <p>
          Insert your email to receive an OPT password
        </p>
        <form noValidate autoComplete="off" style={{display: "flex", flexDirection: "column"}} onSubmit={onSubmit}>
          <TextField autoCapitalize={false} id="email" value={email} onChange={(e) => setEmail(e.target.value)} label="Email" variant="outlined" style={{color: "white", minWidth: 300}} />
          <Button type="submit" style={{marginTop: 20}}>
            {!loading && "Send"}
            {loading && <CircularProgress/>}
          </Button>
        </form>
        </>}
        {step === 1 && <>
        <p>
          Insert your OTP code
        </p>
        <form noValidate autoComplete="off" style={{display: "flex", flexDirection: "column"}} onSubmit={onSubmitOtp}>
          <TextField autoCapitalize={false} id="otp" value={otp} onChange={(e) => setOtp(e.target.value)} label="Otp" variant="outlined" style={{color: "white", minWidth: 300}} />
          <Button type="submit" style={{marginTop: 20}}>
            {!loading && "Send"}
            {loading && <CircularProgress/>}
          </Button>
        </form>
        </>}
        {step === 2 && <>
        <p>
          Your otp is valid!
        </p>
        </>}
        {step === 3 && <>
        <p>
          Your otp is invalid!
        </p>
        </>}
      </header>
    </div>
  );
}

export default App;
